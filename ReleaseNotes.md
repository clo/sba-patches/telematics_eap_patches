# Release Notes

## 0.3 (01-08-2018)
 * First release for meta-qti-boundary.
 * Yocto layer to enable PCIe interface between IMX6 and MDM9x40.
